import React from "react";

export default function Saludar(props){
    const {userInfo, saludarFn} = props;
    const {name = "Anonimo", age} = userInfo;
    
    console.log(props);
    console.log(userInfo);
    return (
        <div>
            <button onClick={()=>saludarFn(name, age)}>Saludar</button>
        </div>
    );
}